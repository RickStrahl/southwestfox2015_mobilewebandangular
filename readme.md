# Creating Mobile Applications with Angular Js and Web Connection

### Session notes, Slides and samples from the Southwest Fox 2015 session.

* [Session Notes](https://bitbucket.org/RickStrahl/southwestfox2015_dotnetwebservices/raw/875e73edf5233d5f5df2ef2e6d3200d26f3753e8/Documents/Strahl_FoxProWebServices.pdf)
* [Session Slides](https://bitbucket.org/RickStrahl/southwestfox2015_dotnetwebservices/raw/875e73edf5233d5f5df2ef2e6d3200d26f3753e8/Documents/Strahl_FoxProDotnetWebServices.pptx)

### AlbumViewer Sample:

* [AlbumViewer Live Site](https://albumviewerswf.west-wind.com) 
* [Source Code for Sample](https://bitbucket.org/RickStrahl/southwestfoxalbumviewer)


Related content:

* [ASP.NET Interop Session Notes](https://bitbucket.org/RickStrahl/southwestfox2015_dotnetwebservices/raw/875e73edf5233d5f5df2ef2e6d3200d26f3753e8/Documents/AspNetInterop/AspNetFoxProRevisited.pdf)