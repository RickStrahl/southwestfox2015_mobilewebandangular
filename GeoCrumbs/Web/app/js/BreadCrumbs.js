/// <reference path="../jquery.js" />
/// <reference path="../ww.jquery.js" />
/// <reference path="geocoder.js" />
function BreadCrumbs() {
    var self = this;
    this.$map = null;

    this.geo = new GeoCoder();

    this.baseUrl = null;
    this.currentUrl = window.location.href;    
    this.noHomeNavigation = false;
    
    this.initialize = function () {        
        // Commonly accessed controls
        self.$map = $("#Map");
        self.$msg = $("#MessageText");
        self.$msgIcon = $("#MessageIcon");
        
        var rsHandler = _.throttle(function () { bc.fitContent(); }, 20);
        $(window).resize(rsHandler);
        setTimeout(bc.fitContent, 100);
    };

    this.getCurrentLocation = function (createMap) {        
        self.geo.getCurrentPosition(function(pos) {
                if (createMap) {
                    self.geo.showMap(bc.$map,
                        pos.Latitude,
                        pos.Longitude, 16,                        
                        function(ps) {
                             getLocation(ps);
                        } //drag callback
                        );
                    
                    hideSplash();
                } else
                    self.moveMapToPosition(pos);

                getLocation(pos);
            },
        function (error) {
            showError(error.message);
        });
    };

    
    this.moveMapToPosition = function(pos, longitude) {
        if (typeof pos === "object")
            self.geo.moveToPosition(pos.Latitude, pos.Longitude);
        else
            self.geo.moveToPosition(pos, longitude);
    };
    
    this.showHistoryItemOnMap = function (loc) {
        if (typeof loc === "string") {
            loc = self.findHistoryItem(loc);
            if (!loc)
                return;
        }
        loc.Updated = new Date();

        self.moveMapToPosition(loc.Latitude, loc.Longitude);        
        updateLocationText(loc, true);
    };

    this.showHistoryDirections = function (id) {
        bc.geo.getCurrentPosition(function (pos) {
            var item;            
            if (id)
                item = self.findHistoryItem(id);
            else
                item = bc.$scope.location;
            
            if (!item)
                return;

            var ua = navigator.userAgent.toLowerCase();

            var mapBaseUrl = "http://maps.google.com?";

            if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1)
                if (!bc.$scope.useGoogleMaps)
                    mapBaseUrl = "http://maps.apple.com?";
                else
                    mapBaseUrl = "comgooglemaps://?";

            var url = mapBaseUrl + "saddr=" + pos.Latitude + "," + pos.Longitude +
                                   "&daddr=" + item.Latitude + "," + item.Longitude + "&directionsmode=driving";
            
            window.location.href = url;            
        },
        function (error) { console.log(error); });
    };
    this.getLocationMapUrl = function(pos) {
        var ua = navigator.userAgent.toLowerCase();
        var url;

        var mapBaseUrl = "http://maps.google.com?";

        if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1)
            if (!bc.$scope.useGoogleMaps)
                mapBaseUrl = "http://maps.apple.com?";
            else
                mapBaseUrl = "comgooglemaps://?";

        else if (ua.indexOf("nokia") > -1) {
            ///mapBaseUrl = "maps:?";
            //url = mapBaseUrl + "cp=" + pos.Longitude + "~" + pos.Latitude;            
            url = "explore-maps://v2.0/show/map/?latlon=" + pos.Latitude + "," + pos.Longitude + "&zoom=18";
            return url;
        }

        url = mapBaseUrl + "q=" + pos.Latitude + "," + pos.Longitude;
        
        return url;
    };
    this.showInMapsApp = function(pos, noNav) {        
        var url = self.getLocationMapUrl(pos);
        window.location.href = url;
    };
    this.sendSms = function(pos) {
        var mapUrl = self.getLocationMapUrl(pos);
        var ua = navigator.userAgent.toLowerCase();
        var url;

        var separator = "?";
        if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1)
            separator = ";";        
        url = "sms:" + separator + "body=" + encodeURIComponent("I'm at " + mapUrl + " @ " + pos.Address);
        
        location.href = url;
    };
    this.findHistoryItem = function(id) {
        var item = _.find(bc.$scope.locationHistory, function(item) {
            return id == item.Id;
        });
        return item;
    };
    
    this.deleteHistoryItem = function (id) {
        if (!id)
            return;        
        bc.$scope.locationHistory = _.reject(bc.$scope.locationHistory, function (item) {
            return item.Id == id;
        });        
    };
    
    this.fitContent = function (sel) {
        sel = sel ? $(sel) : $(".content-area:visible");

        var winHeight = $(window).outerHeight();
        var headerHeight = $(".headerbar:visible").outerHeight();
        var footerHeight = $(".footerbar:visible").outerHeight();
        var messageHeight = $("#Message:visible").outerHeight();

        var pad = sumDimensions(sel, "padding") * 2;

        var newHeight = winHeight - footerHeight - headerHeight - messageHeight - pad;        
        sel.height(newHeight);
    };


    this.showMap = function (hide, selActiveIcon, pos) {
        var $mm = $("#MapContent,#Map");
        if (!hide) {
            $mm.show();
            if (!self.geo.map) {
                bc.fitContent($mm);
                if (pos)
                    self.geo.showMap(bc.$map,
                        pos.Latitude,
                        pos.Longitude, 16);
                if (startLocation.Longitude)
                    self.geo.showMap(bc.$map,
                        startLocation.Latitude,
                        startLocation.Longitude, 16);
                else
                    self.getCurrentLocation(true);
            } else {
                google.maps.event.trigger(bc.$map[0], "resize");
                setTimeout(self.fitContent, 400);
            }
        } else {
            $("#MapContent").hide();
            setTimeout(self.fitContent, 100);
        }

        if (selActiveIcon) {
            $(".iconbutton").removeClass("active");
            $(selActiveIcon).addClass("active");
        }        
    };
}

// our one global var
var bc = new BreadCrumbs();
app.bc = bc;

/// gets a location from a string and assigns it
/// to the current location and updates the display
function getLocation(pos) {    
    bc.geo.getLocation(pos,
        function () {
            updateLocationText(pos);
        },
        function(err) {
            showError(err.message);
        }
    );
}

/// Updates the current location from a location
/// object by assigning values to the scope location
/// and updating the display
function updateLocationText(pos,noAddressFixup) {    
    var ld = bc.$scope.location;
    ld.Longitude = pos.Longitude;
    ld.Latitude = pos.Latitude;

    if (pos.Address) {
        ld.Address = pos.Address;
        if (!noAddressFixup)
            ld.Name = nameFromAddress(ld.Address);
        else
            ld.Name = ld.Address;
    }
    ld.Usage = "OneTime";
    if (!pos.Notes)
        ld.Notes = "";
   if (pos.Address)
       showError(pos.Address, 0, 'fa fa-compass');
   else
       $("#Message").hide();
}

function nameFromAddress(address) {
    if (!address)
        return address;
    var ix = address.indexOf(',');
    if (ix < 1)
        return address;
    var addr = address.substr(0, ix);

    return addr;
}

function showError(message, timeout, icon) {

    bc.$msg.text(message);
    $("#Message").show();

    if (timeout && timeout > 0)
        setTimeout(function() { $("#Message").fadeOut(); }, timeout);
}

