﻿app.controller('mapController', ["$scope", "$routeParams", "locationData",
    function ($scope, $routeParams, locationData) {
        console.log("MapController " +  window.location.hash);
        
        $scope.toCurrentLocation = function () {
            bc.getCurrentLocation(false);
        };
        $scope.showDirections = function () {            
            bc.showHistoryDirections();            
        };
        $scope.showInMapsApp = function () {
            bc.showInMapsApp(locationData.location);
        };
        $scope.sendSms = function () {
            bc.sendSms(locationData.location);
        };
        
        if (window.location.hash != "#/map")
            return;

        $scope.locationData = locationData.location;
        $scope.locationHistory = locationData.locationHistory;

        if ($routeParams.mode == "currentLocation")
            bc.getCurrentLocation(false);
        
        bc.showMap(false, "#LocationIcon");
    }]);
