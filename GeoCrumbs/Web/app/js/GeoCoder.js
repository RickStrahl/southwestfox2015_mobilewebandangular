/// <reference path="../jquery.js" />
/// <reference path="../ww.jquery.js" />

// A Google Maps Helper Class
function GeoCoder() {
    var me = this;

    var _geocoder = new google.maps.Geocoder();
    var _map = null;

    var map, marker;
    this.map =null;
    this.marker = null;
    
    $.extend(this, {
        ScreenWidth: 0,
        ScreenHeight: 0,
        Longitude: 0,
        Latitude: 0,
        Address: null
    });


    // pass in a position object coords.latitude/longitude
    // or pass in an address string
    this.getLocation = function (pos, callback, error) {
        if (!_geocoder)
            _geocoder = new google.maps.Geocoder();

        var gcRequest = {};
        if (typeof pos == "string")
            gcRequest.address = pos; 
        else {
            var latlng = new google.maps.LatLng(pos.Latitude, pos.Longitude);
            gcRequest.latLng = latlng;
        }

        _geocoder.geocode(gcRequest, function (results, status) {
            me.Longitude = 0;
            me.Latitude = 0;
            if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                me.Latitude = results[0].geometry.location.lat();
                me.Longitude = results[0].geometry.location.lng();
                me.Address = results[0].formatted_address;                
                if (callback)
                    callback(me, results, status);
            } else {
                if (error)
                    error({ message: "Location lookup failed." });
            }
        },
            function (status) {
                if (error)
                    error({ message: status });
            });
    };

    this.getCurrentPosition = function (callback, error) {

        if (window.navigator.geolocation) {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                me.Latitude = position.coords.latitude;
                me.Longitude = position.coords.longitude;
                if (callback)
                    callback(me, position);
            },
                function (status) {
                    if (error)
                        error(new CallbackException(status));
                });
        }
    };

    this.showMap = function (sel, lat, long, zoom, dragCallback) {
        if (!zoom)
            zoom = 16;
        var latlng = new google.maps.LatLng(lat, long);
        var opt = {
            zoom: zoom,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var el = $(sel)[0];
        
        if (map) {            
            $("#MapContent").show();
            map.panTo(latlng);
            // trigger a resize
            //setTimeout(function () { google.maps.event.trigger(el, "resize"); }, 200);
            return;
        }

        //var $mc = $("#MapContent");
        //var isVisible = $mc.is(":visible");
        //$mc.show();

        map = new google.maps.Map(el, opt);
        me.map = map;

        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: true,
            disableDefaultUI: false,
        });        
        marker.setTitle("Drag me to change location");
        google.maps.event.addListener(marker, 'dragend', function (e) {
            me.Latitude = e.latLng.lat();
            me.Longitude = e.latLng.lng();
            if (dragCallback)
                dragCallback(me);
        });
        google.maps.event.addListener(map, 'dblclick', function (e) {
            me.moveToPosition(e.latLng.lat(),e.latLng.lng());
            if (dragCallback)
                dragCallback(me);
            return false;
        });            
        me.marker = marker;
        setTimeout(function () { google.maps.event.trigger(el, "resize"); }, 200);
    };
    this.moveToPosition = function (lat, lng) {
        // Update startposition for first load
        window.startLocation.Latitude = lat;
        window.startLocation.Longitude = lng;

        if (!me.map) // map not ready yet
            return;

        var latlng = new google.maps.LatLng(lat, lng);
        marker.setPosition(latlng);

        setTimeout(function() {
            me.map.panTo(latlng);
            me.map.setCenter(latlng);
        },50);
        
        me.Latitude = latlng.lat();
        me.Longitude = latlng.lng();
        
    };
}