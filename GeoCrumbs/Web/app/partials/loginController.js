﻿app.controller('loginController', ["$scope","$location","$http","locationData",
    function($scope, $location, $http, locationData) {
        $scope.validateLogin = function () {
            var el = document.getElementById("LoginForm");            
            if (el.checkValidity && !el.checkValidity()) {                
                showMessage( "Please fill in all fields.",4000);
                return;
            };            
            $http.post("../api/authenticate", $scope.credentials)
                .success(function (token) {
                    locationData.userToken = token;
                    locationData.userEmail = $scope.credentials.username;
                    locationData.saveLocationData();
                    $location.path("/settings");
                })
                .error(function() {
                    showMessage("Invalid password.",4000);                    
                });
        };
        $scope.forgotPasswordLink = function() {
            $scope.forgotPassword = true;          
        };
        $scope.recoverPassword = function () {
            var el = document.getElementById("LoginForm");
            if (el.checkValidity && !el.checkValidity()) {
                showMessage("Please provide a valid email address.", 4000);
                return;
            };
            
            $http.get("../api/account/recoverpassword/" + encodeURIComponent ($scope.credentials.username) + "/")
                .success(function(success) {
                    showMessage("Temporary password has been emailed.",4000,"alert-info");
                })
                .error(function(err) {
                    showMessage("Failed to send password notification.",4000);                    
                });
        };
        function showMessage(msg, ms, info) {
            $scope.message = msg;
            $scope.alertClass = "alert-danger";
            
            if (ms) {
                setTimeout(function() {
                    $scope.message = null;
                    $scope.$apply();
                }, ms);
            }
        }
        
        $scope.forgotPassword = false;
        $scope.message = null;
        $scope.alertClass = "alert-danger";
        
        $scope.credentials = {
            username: null,
            password: null
    };

    // hide the map
    bc.showMap(true,"#LocationIcon");    
}]);