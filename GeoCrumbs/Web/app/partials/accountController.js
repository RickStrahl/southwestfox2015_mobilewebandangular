﻿app.controller('accountController', ["$scope", "$http", "$timeout","$routeParams", "locationData",
    function ($scope, $http, $timeout, $routeParams, locationData) {        

        $scope.processing = true;
        $scope.passwordValidation = null;
        $scope.isChangePassword = false;

        $scope.onChangePassword = function() {
            $scope.isChangePassword = true;
        };

        $scope.saveAccount = function () {
            var userToken = locationData.userToken || "";
            $http.post("../api/account/save/" + userToken , $scope.credentials)
                .success(function(token) {
                    locationData.userToken = token;
                    locationData.useGoogleMaps = $scope.credentials.useGoogleMaps;
                    locationData.saveLocationData();                    
                    location.href = "#/settings";
                })
                .error(function(err, code) {
                    if (code == 401) {
                        location.href = "#/login";
                    }
                    alert("Save operation failed." + err.message);
                });
        };

        $scope.loadUser = function (userToken) {
            $scope.processing = true;

            return $http.get("../api/account/" + userToken)
                .success(function (user) {
                    $scope.processing = false;
                    $scope.credentials = {
                        id: user.id,
                        username: user.username,
                        name: user.name,
                        gravatar: user.username ? app.gravatarLink(user.username, 70) : "",
                        useGoogleMaps: user.useGoogleMaps
                    };
                })
                .error(function(err, code) {
                    if (code == 401)
                        location.href = "#/login";
                })
                .finally(function() { $scope.processing = false; });

        };

        $scope.logout = function() {
            locationData.userToken = "";
            locationData.saveLocationData();
            location.href = "#/map";
        };

        $scope.credentials = {
            username: null,
            password: null,
            password2: null,
            name: null,
            gravatar: null,
            id: null,
        };
        
        
        if (locationData.userToken)
            $scope.loadUser(locationData.userToken);
        else {
            if ($routeParams.mode != "new") {
                location.href = "#/login";
                return;
            }
        }

        // hide the map
        bc.showMap(true, "#LocationIcon");
    }]);