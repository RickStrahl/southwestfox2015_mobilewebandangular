﻿app.controller('searchController', ["$scope", "$location","$timeout","locationData",
    function($scope, $location,$timeout,locationData) {

        console.log("SearchController");
        var $search = $("#SearchLocation");
        $scope.locationData = locationData;
        var autocomplete = null;

        $scope.name = "search";

        $scope.searchLocation = function () {
            // have to read explicitly because Google auto-complete
            // doesn't hit the events Angular is listening to
            //$scope.locationData.searchAddress = $search.val();

            // do this manually with jQUery or google maps bombs with stack overflow
            var pos = $search.val();
            //$scope.locationData.searchAddress;
            
            google.maps.event.clearListeners(autocomplete, "place_changed");

            if ($scope.pickedLocation) {                
                // picked location from autocomplete
                pos = $scope.pickedLocation;                
                $scope.locationData.searchAddress = "";

                bc.$map.show();
                bc.moveMapToPosition(pos.Latitude, pos.Longitude);
                updateLocationText(pos, true);

                $location.path("/map"); 
            }
            else {                
                // look up the location by name and show on map
                bc.geo.getLocation(pos,
                    function (pos) {
                        
                        bc.$map.show();
                        bc.geo.moveToPosition(pos.Latitude, pos.Longitude);
                        updateLocationText(pos, true);

                        getLocation(pos);

                        $timeout(function() { $location.path("/map"); });
                    },
                    function (errmsg) {                  
                        showError(errmsg);
                        bc.showPage();
                    });
            }            
        };
        $scope.backToMap = function () {            
            $location.path("/map");
        };
        $scope.clearSearchAddress = function () {
            $scope.locationData.searchAddress = "";
        };
        $scope.pickedLocation = null;

        $search.focus();

        // HACK: keyboard may force the window to scroll down on focus
        // so let's push it back up 
        $(document.body).scrollTop(0);
        
        
        

        // if you type clear pickedLocation
        $search.keyup(function() { $scope.pickedLocation = null; });
        
        // hide the map        
        bc.showMap(true, "#SearchIcon");

        // Windows Phone doesn't work with Google AutoComplete
        if (!app.isWindowsPhone()) {
            autocomplete = new google.maps.places.Autocomplete($search[0]);

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                console.log("place changed");

                // when I call this and move my map it fails
                var place = autocomplete.getPlace();

                if (place.geometry.location) {
                    var loc = place.geometry.location;                    
                    $scope.pickedLocation = {
                        Latitude: loc.lat(),
                        Longitude: loc.lng(),
                        Address: place.formatted_address,
                        Name: place.name
                    }                    
                    console.log($scope.pickedLocation);

                    $timeout(function() {
                        // submit
                        $scope.searchLocation();
                    },50);
                }
            });
        }
    }]);
