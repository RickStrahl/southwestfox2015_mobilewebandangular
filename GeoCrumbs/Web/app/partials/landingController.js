﻿
app.controller('landingController', ["$scope", "$http", "$routeParams", "$route", "$location", "locationData",
    function ($scope, $http, $routeParams, $route,$location,locationData) {
        console.log("landing controller");

        window.location.href = "../";
        return;
        
        $scope.showMore = function () {
            $scope.moreVisible = !$scope.moreVisible;
        };

        $scope.moreVisible = false;
        $scope.locationData = locationData;

        // hide the map
        bc.showMap(true, "#SettingsIcon");
    }]);