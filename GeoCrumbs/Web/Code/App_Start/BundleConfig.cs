﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace GeoCrumbs
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/controllers")
                   .IncludeDirectory("~/js/controllers","*.js"));

            // Code removed for clarity.
            //BundleTable.EnableOptimizations = true;
        }
    }
}