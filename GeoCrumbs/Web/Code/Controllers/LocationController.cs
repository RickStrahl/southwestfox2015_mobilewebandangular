﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GeoCrumbs.Business;

namespace GeoCrumbs.Controllers
{
    public class LocationsController : ApiController
    {

        /// <summary>
        /// Passes in a list of locations to be merged into
        /// an existing database stored set of locations.
        /// </summary>
        /// <param name="locations"></param>
        /// <returns></returns>
        [HttpPost, Route("api/locations/sync")]
        public IEnumerable<Location> SyncLocations(LocationsSync sync)
        {
            if (sync == null || string.IsNullOrEmpty(UserTokenToUserId(sync.userToken)))
                this.ThrowHttpException(HttpStatusCode.Unauthorized, "Please login first.");

            var userBus = new busUser();

            var merged = userBus.SyncLocations(sync.history, sync.userToken);
            if (merged == null)
                this.ThrowHttpException(HttpStatusCode.InternalServerError, userBus.ErrorMessage);

            return merged.OrderByDescending(loc => loc.Entered);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        [HttpPut, Route("api/locations/delete/{locationId}")]
        public bool DeleteLocation(string locationId, DeleteLocationMessage msg)
        {
            string id = UserTokenToUserId(msg.userToken);
            if (string.IsNullOrEmpty(id))
                throw new ArgumentException("Invalid user token");

            using (var busUser = new busUser())
            {
                var user = busUser.Load(id);
                var loc = user.Locations.FirstOrDefault(lc => lc.Id == locationId);

                if (loc == null)
                    this.ThrowHttpException(HttpStatusCode.NotFound, "Invalid location id provided.");

                loc.Deleted = true;

                return busUser.Save(user);
            }
        }

        public class DeleteLocationMessage
        {
            public string locationId { get; set; }
            public string userToken { get; set; }
        }


        [HttpGet, Route("api/locations/{userToken}")]
        public IEnumerable<Location> GetLocationsForUser(string userToken)
        {            
            string id = UserTokenToUserId(userToken);

            var userBus = new busUser();
            var user = userBus.Load(id);
            if (user == null)
                new ArgumentException("Invalid user token");

            return userBus.GetRecentLocations(id);
        }

        string UserTokenToUserId(string token)
        {
            var userBus = new busUser();
            string id = userBus.TokenToUserId(token);
            if (string.IsNullOrEmpty(id))
                this.ThrowHttpException(HttpStatusCode.Unauthorized, "Invalid user id.");

            return id;
        }

    }
}
