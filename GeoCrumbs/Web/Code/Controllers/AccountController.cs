﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GeoCrumbs.Business;

namespace GeoCrumbs.Controllers
{
    public class AccountController : ApiController
    {
        /// <summary>
        /// Method authenticates a user and creates a user token
        /// </summary>
        /// <returns></returns>
        
        [HttpGet,HttpPost,Route("api/authenticate")]    
        public string Authenticate(UserCredentials credentials)
        {
            if (credentials == null)
                this.ThrowHttpException(HttpStatusCode.Unauthorized, "No credentials passed.");

            busUser userBus = new busUser();
            var user = userBus.AuthenticateAndLoad(credentials.username, credentials.password);
            if (user == null)
                this.ThrowHttpException(HttpStatusCode.Unauthorized, userBus.ErrorMessage);

            string token = userBus.CreateToken(user.Id);
            if (token == null)
                this.ThrowHttpException(HttpStatusCode.Unauthorized, userBus.ErrorMessage);

            return token;
        }

        [HttpGet]
        [Route("api/account/{userToken}")]
        public UserCredentials GetUser(string userToken)
        {
            string userId = UserTokenToUserId(userToken);
            //if (string.IsNullOrEmpty(userId))
            //    this.ThrowHttpException(HttpStatusCode.Unauthorized, "Acess denied. Invalid user token.");

            var userBus = new busUser();

            var user = userBus.TokenToUser(userToken);
            if (user == null)
                user = userBus.NewEntity();

            user.Password = null;
            user.Locations = null;

            return new UserCredentials()
            {
                 id = user.Id,
                 name = user.Alias,
                 username = user.Email,
                 useGoogleMaps = user.UseGoogleMaps                  
            };
        }

        [HttpPost,HttpPut,Route("api/account/save/{userToken?}")]
        public string Save(UserCredentials msg, string userToken = null)
        {            
            var userBus = new busUser();

            User user = null;

            if (!string.IsNullOrEmpty(msg.id))
            {
                string userId = UserTokenToUserId(userToken);
                user = userBus.Load(msg.id);
                if (user == null || user.Id != userId)
                    this.ThrowHttpException(HttpStatusCode.NotFound, "Invalid user id - can't save this user.");
            }
            else
            {
                if (string.IsNullOrEmpty(userToken))
                    user = userBus.NewEntity();
                else
                {
                    this.ThrowHttpException(HttpStatusCode.Conflict,
                        "Invalid user token for new entry. Please log out first.");                    
                }
            }

            user.UserName = msg.username;
            user.Email = msg.username;
            user.UseGoogleMaps = msg.useGoogleMaps;

            if (!string.IsNullOrEmpty(msg.password))
                user.Password = msg.password;

            user.Alias = msg.name;
            user.Updated = DateTime.UtcNow;

            if (!userBus.Save())
                this.ThrowHttpException(HttpStatusCode.InternalServerError,
                    "Unable to save user: " + userBus.ErrorMessage);

            var token = userBus.CreateToken(user.Id);

            return token;
        }

        [HttpGet,Route("api/account/recoverpassword/{userName}")]
        public bool RecoverPassword(string userName)
        {            
            var userBus = new busUser();
            return userBus.RecoverPassword(userName);
        }

        /// <summary>
        /// Converts a user Token to a User Id and 
        /// automatically throws an exception if the conversion
        /// doesn't find a valid key.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        string UserTokenToUserId(string token)
        {
            var userBus = new busUser();
            string id = userBus.TokenToUserId(token);
            if (string.IsNullOrEmpty(id))
                this.ThrowHttpException(HttpStatusCode.Unauthorized, "Invalid user id.");

            return id;
        }
    }

    public class UserCredentials
    {
        public string username { get; set; }        
        public string password { get; set; }
        public string password2 { get; set;  }
        public string originalPassword { get; set; }
        public bool useGoogleMaps { get; set; }
        public string name {get; set; }
        public string id { get; set; }
    }

    
}
