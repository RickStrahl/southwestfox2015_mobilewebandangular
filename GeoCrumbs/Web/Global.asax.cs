﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Westwind.Web;

namespace GeoCrumbs
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            ScriptLoader.jQueryUiCdnUrl = "//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js";
            ScriptLoader.jQueryCdnFallbackUrl = "~/scripts/jquery.min.js";
        }
    }
}
